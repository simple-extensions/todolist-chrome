chrome.runtime.onInstalled.addListener(() => {
    chrome.storage.local.set({ list: [] }, () => console.log("List Ready"));
    chrome.declarativeContent.onPageChanged.removeRules(undefined, _ => {
        chrome.declarativeContent.onPageChanged.addRules([rule1]);
    });
});

var rule1 = {
    conditions: [
        new chrome.declarativeContent.PageStateMatcher({
            pageUrl: { schemes: ['http', 'https', 'chrome'] }
        })
    ],
    actions: [new chrome.declarativeContent.ShowPageAction()]
}